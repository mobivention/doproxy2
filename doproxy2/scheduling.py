from rq_scheduler import Scheduler
from rq import Worker
from redis import Redis
import yaml
from datetime import datetime
from dateutil.parser import parse
from .doproxy import DOProxy
from pytz import timezone, UTC


def read_config():

    with open("doproxy.yml") as f:
        config = yaml.load(f)
    return config

config = read_config()
redis = Redis(host=config['redis']['host'], port=config['redis']['port'], password=config['redis'].get('password', None), db=config['redis'].get('db', 0))
scheduler = Scheduler(connection=redis) # Get a scheduler for the "default" queue


def schedule_jobs():
    for j in scheduler.get_jobs():
        scheduler.cancel(j)
    scheduler.schedule(scheduled_time=datetime.utcnow(),
                       func=check,
                       args=[],
                       interval=60,
                       repeat=None)


def check():
    config = read_config()
    scheduling_mode = config['scheduling']['mode']
    if scheduling_mode == 'times':
        _establish_droplets(_get_droplets_for_current_time)
    elif scheduling_mode == 'weekdays':
        _establish_droplets(_get_droplets_for_current_time_weekdays)
    elif scheduling_mode == 'tippspiel':
        from .tippspiel import establish_droplets
        establish_droplets()
    else:
        raise Exception('Invalid scheduling mode')


def run_scheduler():
    scheduler.run()


def run_worker():
    w = Worker(['default'], connection=redis)
    w.work()


def _get_droplets_for_current_time():
    config = read_config()
    scheduling_input = config['scheduling']['input']
    with open(scheduling_input) as f:
        lines = f.readlines()

        for idx, l in enumerate(lines):
            t, n = l.split(',')
            time = parse(t, dayfirst=False)
            next_time = None
            if idx + 1 < len(lines):
                next_row = lines[idx+1]
                next_time = parse(next_row.split(',')[0], dayfirst=False)
            if not next_time or next_time > datetime.now(UTC):
                return int(n)


def _establish_droplets(func):
    n = func()
    proxy = DOProxy()
    proxy.set_count(n)


def _get_droplets_for_current_time_weekdays():
    config = read_config()
    scheduling_input = config['scheduling']['input']
    with open(scheduling_input) as f:
        lines = f.readlines()

        valid_counts = []

        for idx, l in enumerate(lines):
            if l.startswith('#'):
                continue
            day, hour, tz, count = l.split(',')
            zone = timezone(tz)
            now = datetime.now(zone)
            if now.isoweekday() == int(day) and now.hour >= int(hour):
                valid_counts.append(count)

        if len(valid_counts) > 0:
            return int(valid_counts.pop())


