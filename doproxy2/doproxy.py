import yaml
import digitalocean
from typing import List
import time
import jinja2
import shutil
import os
import subprocess
import shlex


class DOProxy:

    def __init__(self):

        with open("doproxy.yml") as f:
            config = yaml.load(f)

            self.jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader('.'))

            self.ssh_key_ids = config['ssh_key_ids']
            self.inventory_file = config['inventory_file']
            self.userdata_file = config['userdata_file']
            self.nginx_template_file = config['nginx_template_file']
            self.nginx_cfg_file = config['nginx_cfg_file']
            self.nginx_cfg_path = config['nginx_cfg_path']
            self.nginx_active = config['nginx_active']
            self.nginx_reload_command = config['nginx_reload_command']
            self.hostname_prefix = config['droplet_options']['hostname_prefix']
            self.region = config['droplet_options']['region']
            self.size = config['droplet_options']['size']
            self.image = config['droplet_options']['image']
            self.token = config['token']
            self.client = digitalocean.Manager(token=self.token)

            self.droplets = []  # type: List[digitalocean.Droplet]
            self.get_inventory()

    def get_inventory(self):

        def get_droplet(id):
            d = self.client.get_droplet(id)
            return d
        try:
            with open(self.inventory_file) as f:
                lines = [l for l in f.read().split("\n") if l != ""]
                self.droplets = [get_droplet(id) for id in lines]
        except FileNotFoundError:
            print('Inventory file not found. Please check your config')

    def print_inventory(self):
        for idx, d in enumerate(self.droplets):
            print("{i}) {name} (pvt.ip: {ip}, status: {status}, id: {id}".format(
                i=idx,
                name=d.name,
                ip=d.private_ip_address,
                status=d.status,
                id=d.id
            ))

    def write_inventory(self):
        with open(self.inventory_file, 'w') as inventory:
            ids = [d.id for d in self.droplets]
            inventory.write("\n".join([str(i) for i in ids]))

    def create_server(self):
        with open(self.userdata_file) as ud:
            hostname = "{prefix}-{count}".format(prefix=self.hostname_prefix, count=len(self.droplets))
            userdata = ud.read()
            droplet = digitalocean.Droplet(token=self.token,
                                           name=hostname,
                                           region=self.region,
                                           image=self.image,
                                           size_slug=self.size,
                                           backups=False,
                                           private_networking=True,
                                           ssh_keys=self.ssh_key_ids,
                                           user_data=userdata)
            droplet.create()
            id = droplet.id
            # if droplet.status == 'new':
            while droplet.status != 'active':
                time.sleep(15)
                droplet = self.client.get_droplet(id)
            self.droplets.append(droplet)
            self.write_inventory()
            self.reload_nginx()
            print('Success: {id} created and added to backend'.format(id=id))
            # else:
            #     print('Error has occured. Status was {}'.format(droplet.status))

    def delete_server(self, line_number):
        if line_number >= len(self.droplets):
            print('Specified line does not exist in inventory')
            return
        else:
            with open(self.inventory_file, 'w') as inventory:
                id = self.droplets[line_number].id
                d = self.droplets.pop(line_number)
                self.write_inventory()
                d.destroy()
                print("Droplet {} deleted and removed from backend".format(id))
                self.reload_nginx()

    def generate_nginx_cfg(self):
        template = self.jinja_env.get_template('nginx.conf.jinja2')
        out = template.render(droplets=self.droplets)
        with open(self.nginx_cfg_file, 'w') as f:
            f.write(out)

    def reload_nginx(self):
        self.generate_nginx_cfg()
        if self.nginx_active:
            print("Copying nginx.cfg")
            shutil.copy(self.nginx_cfg_file, os.path.join(self.nginx_cfg_path, self.nginx_cfg_file))
            subprocess.Popen(shlex.split(self.nginx_reload_command))

    def set_count(self, target):
        count = len(self.droplets)
        if count < target:
            to_create = target - count
            print("Create {} droplets".format(to_create))
            [self.create_server() for _ in range(to_create)]
        elif count > target:
            to_delete = count - target
            print("Delete {} droplets".format(to_delete))
            [self.delete_server(i) for i in range(count - 1, target - 1, -1)]

    def __repr__(self):
        return """
        SSH Key Ids: {ssh_key_ids}
        """.format(ssh_key_ids=self.ssh_key_ids)