import click
from .doproxy import DOProxy
from .scheduling import run_scheduler, run_worker, schedule_jobs

proxy = DOProxy()

@click.group()
def cli():
    pass


@cli.group()
def droplet():
    pass


@droplet.command()
def create():
    proxy.create_server()


@droplet.command(name='print')
def print_inventory():
    proxy.print_inventory()


@droplet.command()
@click.argument('line_number', type=int)
def delete(line_number=0):
    proxy.delete_server(line_number)


@droplet.command()
def reload():
    proxy.reload_nginx()


@droplet.command()
@click.argument('target', type=int)
def set_count(target):
    proxy.set_count(target)


@droplet.command()
def generate():
    proxy.generate_nginx_cfg()

@cli.group()
def queue():
    pass

@queue.command()
def work():
    run_worker()

@queue.command()
def schedule():
    run_scheduler()

@queue.command()
def setup():
    schedule_jobs()

if __name__ == '__main__':
    cli()