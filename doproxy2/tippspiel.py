from datetime import datetime, timedelta
import yaml
from .doproxy import DOProxy
try:
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker
    from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime, Float
    from sqlalchemy.ext.declarative import declarative_base
    from sqlalchemy.orm import relationship
except ImportError:
    print("You need to install sqlalchemy and an appropriate sql library to use the Tippspiel Scheduler")

Base = declarative_base()

with open("doproxy.yml") as f:
    config = yaml.load(f)

class GameCategory(Base):
    __tablename__ = 'GameCategory'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column('id', Integer, primary_key=True)
    name = Column('name', String(200), nullable=True)
    nr = Column('nr', Integer, nullable=False, index=True)
    active = Column('active', Boolean, nullable=False, default=0)
    competition_id = Column('competition_id', Integer, ForeignKey('Competition.id'))
    cat_type = Column('cat_type', Integer, default=1, server_default='1', index=True)


class Game(Base):
    __tablename__ = 'Game'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column('id', Integer, primary_key=True)
    home_team_id = Column('home_team_id', Integer, ForeignKey('Team.id'))
    guest_team_id = Column('guest_team_id', Integer, ForeignKey('Team.id'))
    kickoff = Column('kickoff', DateTime)
    category_id = Column('category_id', Integer, ForeignKey('GameCategory.id'))
    category = relationship('GameCategory', foreign_keys=[category_id], backref='games')
    push_sent = Column('push_sent', Boolean, default=0)
    bet_tendency = Column('bet_tendency', Float, default=0.5, server_default='0.5')

engine = create_engine(config['tippspiel']['database'])
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()


def find_games():

    now = datetime.utcnow()
    competitions = config['tippspiel']['competitions']
    games = session.query(Game).join(Game.category)\
        .filter(GameCategory.competition_id.in_(competitions))\
        .filter(Game.kickoff < now - timedelta(minutes=95)) \
        .filter(now - timedelta(minutes=165) < Game.kickoff) \
        .order_by(Game.kickoff).all()
    return games


def establish_droplets():
    g = find_games()
    print("Active Games: %s" % len(g))
    count = config['tippspiel']['idle_servers'] if len(g) == 0 else config['tippspiel']['busy_servers']
    print("Droplets needed: %s" % count)
    proxy = DOProxy()
    proxy.set_count(count)

if __name__ == '__main__':
    find_games()