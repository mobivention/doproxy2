from setuptools import setup
import os

extras_require = {
    'tippspiel': [
        'sqlalchemy',
    ]
}


data_files = [(os.getcwd(), ['doproxy.yml.sample', 'nginx.conf.jinja2.sample', 'user-data.yml.sample', 'times.csv.sample', 'weekdays.csv.sample'])]

setup(
    author='Soeren Busch',
    author_email='sbusch@mobivention.com',
    url='https://bitbucket.org/mobivention/doproxy2',
    name='DOProxy2',
    version='0.1.5',
    packages=['doproxy2'],
    install_requires=[
        'Click',
        'python-digitalocean',
        'pytz',
        'pyyaml',
        'jinja2',
        'rq-scheduler',
        'python-dateutil',
        'pytz',
        'typing'
    ],
    entry_points='''
        [console_scripts]
        doproxy2=doproxy2:cli
    ''',
    data_files=data_files
)